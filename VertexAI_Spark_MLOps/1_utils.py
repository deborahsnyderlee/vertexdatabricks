# Databricks notebook source
# note: if you're NOT using ML type cluster, need to have manually installed mlflow on the cluster 
import pandas as pd
import numpy as np
import matplotlib as plt
import os
import sys
import functools
import warnings
from pyspark import *
from pyspark.sql.functions import *
from pyspark import since, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.evaluation import RegressionEvaluator
import matplotlib.pyplot as plt
from pyspark.rdd import PythonEvalType
from pyspark.sql.column import Column, _to_java_column, _to_seq, _create_column_from_literal
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType, DataType
# Keep UserDefinedFunction import for backwards compatible import; moved in SPARK-22409
from pyspark.sql.udf import UserDefinedFunction, _create_udf  # noqa: F401
from pyspark.sql.udf import _create_udf
# Keep pandas_udf and PandasUDFType import for backwards compatible import; moved in SPARK-28264
from pyspark.sql.pandas.functions import pandas_udf, PandasUDFType  # noqa: F401
from pyspark.sql.utils import to_str
from pyspark.sql.functions import col,isnan, when, count, expr
import mlflow
import mlflow.spark
from pyspark.ml import Pipeline
from pyspark.sql.types import StructType,StructField,LongType, StringType,DoubleType,TimestampType, IntegerType, BooleanType, DecimalType

import mlflow
import mlflow.spark
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import FeatureHasher


from pyspark.ml import Pipeline
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
import mlflow
import mlflow.spark
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import FeatureHasher
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.regression import GBTRegressor

import pandas as pd
import numpy as np
import matplotlib as plt
import os
import sys
import functools
import warnings
from pyspark import *
from pyspark.sql.functions import *
from pyspark import since, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.evaluation import RegressionEvaluator
import matplotlib.pyplot as plt
from pyspark.rdd import PythonEvalType
from pyspark.sql.column import Column, _to_java_column, _to_seq, _create_column_from_literal
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType, DataType
# Keep UserDefinedFunction import for backwards compatible import; moved in SPARK-22409
from pyspark.sql.udf import UserDefinedFunction, _create_udf  # noqa: F401
from pyspark.sql.udf import _create_udf
# Keep pandas_udf and PandasUDFType import for backwards compatible import; moved in SPARK-28264
from pyspark.sql.pandas.functions import pandas_udf, PandasUDFType  # noqa: F401
from pyspark.sql.utils import to_str
from pyspark.sql.functions import col,isnan, when, count, expr
import mlflow
import mlflow.spark
import uuid
import time
from mlflow.tracking import MlflowClient
import mlflow.pyfunc
from pyspark.sql.functions import struct

print("Libraries installed")

# COMMAND ----------

# CHANGE THESE TO FILE LOCATION AND FILE NAMES USED WHEN UPLOADING
file_path = "/FileStore/tables/rawfiles/"
sales_tablename = "sales_data_set"
features_tablename = "Features_data_set"
stores_tablename = "stores_data_set"

# CHANGE THESE TO WHATEVER NEW SCHEMA NAME AND TABLE NAME DESIRED
final_dataset_schemaname = "vertexdatabricks"
final_dataset_tablename = "kaggle_retail_dataset"

# CHANGE THESE TO REGISTER DIFFERENT MODEL USING MLFLOW API
run_id = "299fcd464d6149c8becfd797084ef58b"
registered_model_name = "Weekly_Sales_GBTR_model"

# WHEN STREAMING DATASET IN SPECIFY MODEL STAGE TO LOAD IN
model_stage="Production"

print("Variables created")

# COMMAND ----------

# function to generate plots


# COMMAND ----------

# hyperparam tuning

