# Databricks notebook source
# MAGIC %md
# MAGIC # Import Libraries

# COMMAND ----------

import pandas as pd
import numpy as np
import matplotlib as plt
import os
import sys
import functools
import warnings
from pyspark import *
from pyspark.sql.functions import *
from pyspark import since, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.evaluation import RegressionEvaluator
import matplotlib.pyplot as plt
from pyspark.rdd import PythonEvalType
from pyspark.sql.column import Column, _to_java_column, _to_seq, _create_column_from_literal
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType, DataType
# Keep UserDefinedFunction import for backwards compatible import; moved in SPARK-22409
from pyspark.sql.udf import UserDefinedFunction, _create_udf  # noqa: F401
from pyspark.sql.udf import _create_udf
# Keep pandas_udf and PandasUDFType import for backwards compatible import; moved in SPARK-28264
from pyspark.sql.pandas.functions import pandas_udf, PandasUDFType  # noqa: F401
from pyspark.sql.utils import to_str
from pyspark.sql.functions import col,isnan, when, count, expr
import mlflow
import mlflow.spark

# COMMAND ----------

# MAGIC %md
# MAGIC # Import Data into Spark DF

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC Data source: https://www.kaggle.com/manjeetsingh/retaildataset?select=sales+data-set.csv
# MAGIC <br><br>
# MAGIC Content
# MAGIC <br>
# MAGIC Context
# MAGIC The Challenge - One challenge of modeling retail data is the need to make decisions based on limited history. Holidays and select major events come once a year, and so does the chance to see how strategic decisions impacted the bottom line. In addition, markdowns are known to affect sales – the challenge is to predict which departments will be affected and to what extent.
# MAGIC 
# MAGIC Content
# MAGIC You are provided with historical sales data for 45 stores located in different regions - each store contains a number of departments. The company also runs several promotional markdown events throughout the year. These markdowns precede prominent holidays, the four largest of which are the Super Bowl, Labor Day, Thanksgiving, and Christmas. The weeks including these holidays are weighted five times higher in the evaluation than non-holiday weeks.
# MAGIC 
# MAGIC Within the Excel Sheet, there are 3 Tabs – Stores, Features and Sales
# MAGIC 
# MAGIC Stores
# MAGIC Anonymized information about the 45 stores, indicating the type and size of store
# MAGIC 
# MAGIC Features
# MAGIC Contains additional data related to the store, department, and regional activity for the given dates.
# MAGIC 
# MAGIC Store - the store number
# MAGIC Date - the week
# MAGIC Temperature - average temperature in the region
# MAGIC Fuel_Price - cost of fuel in the region
# MAGIC MarkDown1-5 - anonymized data related to promotional markdowns. MarkDown data is only available after Nov 2011, and is not available for all stores all the time. Any missing value is marked with an NA
# MAGIC CPI - the consumer price index
# MAGIC Unemployment - the unemployment rate
# MAGIC IsHoliday - whether the week is a special holiday week
# MAGIC Sales
# MAGIC Historical sales data, which covers to 2010-02-05 to 2012-11-01. Within this tab you will find the following fields:
# MAGIC 
# MAGIC Store - the store number
# MAGIC Dept - the department number
# MAGIC Date - the week
# MAGIC Weekly_Sales -  sales for the given department in the given store
# MAGIC IsHoliday - whether the week is a special holiday week
# MAGIC The Task
# MAGIC Predict the department-wide sales for each store for the following year
# MAGIC Model the effects of markdowns on holiday weeks
# MAGIC Provide recommended actions based on the insights drawn, with prioritization placed on largest business impact

# COMMAND ----------

#function to loop through and check nulls in columns in dataframes to only keep what's useful
def check_nulls(dataframes):
    for name in dataframes:
        df_Columns=name.columns
        df=name
        print(name)
        df.select([count(when(isnan(c) | col(c).isNull(), c)).alias(c) for c, t in df.dtypes if t != "timestamp"]).show()

# COMMAND ----------

check_nulls(dataframes)

# COMMAND ----------

# read table into spark df
features_df = sqlContext.table("vertexdatabricks.kaggle_ra_features")
sales_df = sqlContext.table("vertexdatabricks.kaggle_ra_sales_data")
stores_df = sqlContext.table("vertexdatabricks.kaggle_ra_stores")

# COMMAND ----------

sales_df.printSchema()

# COMMAND ----------

# MAGIC %sql
# MAGIC DROP TABLE IF EXISTS vertexdatabricks.kaggle_ra_traindata

# COMMAND ----------

# MAGIC %sql
# MAGIC --combine tables together update schema, create dates, remove duplicate keys
# MAGIC 
# MAGIC CREATE OR REPLACE TABLE vertexdatabricks.kaggle_ra_traindata AS
# MAGIC SELECT 
# MAGIC sa.Store,
# MAGIC sa.Dept,
# MAGIC CAST(LEFT(sa.Date, 2) AS int) AS day,
# MAGIC CAST(SUBSTRING(sa.Date, 4, 2) AS int) AS month,
# MAGIC CAST(RIGHT(sa.Date, 4) AS int) AS year,
# MAGIC sa.Weekly_Sales,
# MAGIC sa.IsHoliday,
# MAGIC f.Temperature,
# MAGIC f.Fuel_Price,
# MAGIC f.MarkDown1,
# MAGIC f.MarkDown2,
# MAGIC f.MarkDown3,
# MAGIC f.MarkDown4,
# MAGIC f.MarkDown5,
# MAGIC f.CPI,
# MAGIC f.Unemployment,
# MAGIC st.Type,
# MAGIC CAST(st.Size AS decimal) AS Size
# MAGIC FROM vertexdatabricks.kaggle_ra_sales_data as sa
# MAGIC INNER JOIN vertexdatabricks.kaggle_ra_features as f on (f.Store=sa.Store AND f.Date=sa.Date)
# MAGIC INNER JOIN vertexdatabricks.kaggle_ra_stores as st ON st.Store=sa.Store

# COMMAND ----------

# cast Size from int to double type, read into Spark
kaggle_ra_trainingdata_df =sqlContext.table("vertexdatabricks.kaggle_ra_traindata")
kaggle_ra_trainingdata_df=kaggle_ra_trainingdata_df.withColumn("Size_double", kaggle_ra_trainingdata_df['Size'].cast("double"))

# COMMAND ----------

kaggle_ra_trainingdata_df.columns

# COMMAND ----------

kaggle_ra_trainingdata_df.printSchema()

# COMMAND ----------

# feature hasher for input features https://spark.apache.org/docs/3.1.1/api/python/reference/api/pyspark.ml.feature.FeatureHasher.html

df2=kaggle_ra_trainingdata_df
from pyspark.ml.feature import FeatureHasher
hasher = FeatureHasher(inputCols=['IsHoliday', 'day', 'month', 'year', 'Temperature', 'Fuel_Price', 'Size_double', 'Store', 'Dept', 'MarkDown1', 'MarkDown2', 'MarkDown3', 'MarkDown4', 'MarkDown5', 'CPI', 'Unemployment', 'Type'],
                       outputCol="features")

featurized_df = hasher.transform(df2)
featurized_df.show(truncate=False)

# COMMAND ----------

featurized_df.printSchema()

# COMMAND ----------

# convert from dense to sparse vector, vector assembler
df3=featurized_df
feat_cols=["features"]
from pyspark.ml.feature import VectorAssembler
assembler = VectorAssembler(inputCols=feat_cols, outputCol="features_dense")
df4 = assembler.transform(df3).select('features_dense','Weekly_Sales')
df4.show(3)

# COMMAND ----------

# split into test and train datasets
(train, test) = df4.randomSplit([0.8, 0.2])

# COMMAND ----------

# model training
gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=10)
gbtr = gbtr.fit(train)

# COMMAND ----------

# evaluate model using test dataset
mdata = gbtr.transform(test)
mdata.show(3)
 
rmse=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="rmse")
rmse=rmse.evaluate(mdata) 
 
mae=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="mae")
mae=mae.evaluate(mdata) 
 
r2=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="r2")
r2=r2.evaluate(mdata)

print("RMSE: ", rmse)
print("MAE: ", mae)
print("R-squared: ", r2)

# COMMAND ----------

# visualize results
mdata.show()

# COMMAND ----------

gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=1)
model = gbtr.fit(train)
print("GBTRegressor parameters:\n" + gbtr.explainParams() + "\n")

# COMMAND ----------

# MAGIC %md
# MAGIC # pipeline with logging

# COMMAND ----------

# cast Size from int to double type, read into Spark
kaggle_ra_trainingdata_df =sqlContext.table("vertexdatabricks.kaggle_ra_traindata")
kaggle_ra_trainingdata_df=kaggle_ra_trainingdata_df.withColumn("Size_double", kaggle_ra_trainingdata_df['Size'].cast("double"))

# COMMAND ----------

# split train and test data
(train, test) = kaggle_ra_trainingdata_df.randomSplit([0.8, 0.2])

# COMMAND ----------

import mlflow
import mlflow.spark
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import FeatureHasher

#enable MLFlow autologging
mlflow.autolog(exclusive=False)

#start MLFlow run
with mlflow.start_run(run_name='Weekly_Sales_GBTR') as run:
    #add hasher
    hasher = FeatureHasher(inputCols=['IsHoliday', 'day', 'month', 'year', 'Temperature', 'Fuel_Price', 'Size_double', 'Store', 'Dept', 'MarkDown1', 'MarkDown2', 'MarkDown3', 'MarkDown4', 'MarkDown5', 'CPI', 'Unemployment', 'Type'],
                       outputCol="features")
    
    # [old] assembler = VectorAssembler(inputCols= ['x'], outputCol="raw_features")
    
    feat_cols=["features"]
    assembler = VectorAssembler(inputCols=feat_cols, outputCol="features_dense")
    
    #[old] lr = LinearRegression(featuresCol='features', labelCol="y")
    gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=1)
    
    #print out parameter info
    print("GBTRegressor parameters:\n" + gbtr.explainParams() + "\n")
    
    #define pipeline
    pipeline = Pipeline(stages=[hasher, assembler, gbtr])
    #[old] lrModel = pipeline.fit(train_data)
    gbtr_model = pipeline.fit(train)
    
    # predictions
    #[old] lr_predictions = lrModel.transform(test_data)[['Brand_Geo' , 'ULSubCategoryText','TimeDescription','x','y','prediction']]
    gbtr_predictions = gbtr_model.transform(test)

    # Logging model
    #[old] mlflow.spark.log_model(lrModel, "LinearRegression-spark-model")
    mlflow.spark.log_model(gbtr_model, "Weekly_Sales_GBTR_model")

    # Logging params
    # [old] slope_reg = lrModel.stages[-1].intercept
    # [old] mlflow.log_param("Slope",slope_reg)
    
    # print("Model was fit using parameters: ")
    # print(model1.extractParamMap())
    
    # [old] intercept_reg = lrModel.stages[-1].coefficients[0]
    # [old] mlflow.log_param("Intercept",intercept_reg)

    # Logging metrics
    # [old] rmseEvaluator = RegressionEvaluator(predictionCol="prediction", labelCol="y", metricName="rmse")
    # [old] rmse = rmseEvaluator.evaluate(lr_predictions)
    # [old] mlflow.log_metric("RMSE", rmse)

    # [old] r2Evaluator = RegressionEvaluator(predictionCol="prediction", labelCol="y", metricName="r2")
    # [old] r2 = r2Evaluator.evaluate(lr_predictions)
    # [old] mlflow.log_metric("R-Square", r2)

    # [old] maeEvaluator = RegressionEvaluator(predictionCol="prediction", labelCol="y", metricName="mae")
    # [old] mae = maeEvaluator.evaluate(lr_predictions)
    # [old] mlflow.log_metric("MAE", mae)
  
    # log the rmse
    rmse=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="rmse")
    rmse=rmse.evaluate(gbtr_predictions) 
    mlflow.log_metric("RMSE", rmse)
    
    # log mae
    mae=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="mae")
    mae=mae.evaluate(gbtr_predictions) 
    mlflow.log_metric("MAE", mae)
    
    # log r^2
    r2=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="r2")
    r2=r2.evaluate(gbtr_predictions)
    mlflow.log_metric("R-Square", r2)
    
    # Logging image as artifact
    # [old] image = plot_linreg(Brand_GeoDesc)
    # [old] mlflow.log_artifact("Linear-Regression-plot.png")

# COMMAND ----------


