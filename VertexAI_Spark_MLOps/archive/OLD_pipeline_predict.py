# Databricks notebook source
# MAGIC %md
# MAGIC # pipeline to predict (score) on test data

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# only run below command if cluster stopped after you ran preprocessing 

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/recreate_traintest_df

# COMMAND ----------

# enable MLFlow autologging
mlflow.autolog(exclusive=False)

# start MLFlow run
with mlflow.start_run(run_name='Weekly_Sales_GBTR') as run:
    
    # add hasher
    hasher = FeatureHasher(inputCols=['IsHoliday', 'day', 'month', 'year', 'Temperature', 'Fuel_Price', 'Size_double', 'Store', 'Dept', 'MarkDown1', 'MarkDown2', 'MarkDown3', 'MarkDown4', 'MarkDown5', 'CPI', 'Unemployment', 'Type'],
                       outputCol="features")
    
    # add assembler
    feat_cols=["features"]
    assembler = VectorAssembler(inputCols=feat_cols, outputCol="features_dense")
    
    # model definition with parameters
    gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=1)
    
    # define pipeline
    pipeline = Pipeline(stages=[hasher, assembler, gbtr])
    
    # training pipeline
    gbtr_model = pipeline.fit(train)
    
    # predictions on test data
    gbtr_predictions = gbtr_model.transform(test)

    # log model
    mlflow.spark.log_model(gbtr_model, "Weekly_Sales_GBTR_model")

    # Logging params handled by MLFlow autologger

    # Logging evaluation metrics
  
    # log the rmse
    rmse=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="rmse")
    rmse=rmse.evaluate(gbtr_predictions) 
    mlflow.log_metric("RMSE", rmse)
    
    # log mae
    mae=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="mae")
    mae=mae.evaluate(gbtr_predictions) 
    mlflow.log_metric("MAE", mae)
    
    # log r^2
    r2=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="r2")
    r2=r2.evaluate(gbtr_predictions)
    mlflow.log_metric("R-Square", r2)
    
    # Logging image as artifact
    # [old] image = plot_linreg(Brand_GeoDesc)
    # [old] mlflow.log_artifact("Linear-Regression-plot.png")

# COMMAND ----------


