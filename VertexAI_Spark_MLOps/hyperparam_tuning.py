# Databricks notebook source
# MAGIC %md
# MAGIC # hyperparameter tuning 

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/recreate_traintest_df

# COMMAND ----------

# enable MLFlow autologging
mlflow.autolog(exclusive=False)

# start MLFlow run
with mlflow.start_run(run_name='Weekly_Sales_HPTuning') as run:
    
    # add hasher
    hasher = FeatureHasher(inputCols=['IsHoliday', 'day', 'month', 'year', 'Temperature', 'Fuel_Price', 'Size_double', 'Store', 'Dept', 'MarkDown1', 'MarkDown2', 'MarkDown3', 'MarkDown4', 'MarkDown5', 'CPI', 'Unemployment', 'Type'],
                       outputCol="features")
    
    # add assembler
    feat_cols=["features"]
    assembler = VectorAssembler(inputCols=feat_cols, outputCol="features_dense")
    
    # estimator definition with parameters
    gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=1)
    
    # Define a grid of hyperparameters to test:
    #  - maxDepth: max depth of each decision tree in the GBT ensemble
    #  - maxIter: iterations, i.e., number of trees in each GBT ensemble
    # In this example notebook, we keep these values small.  In practice, to get the highest accuracy, you would likely want to try deeper trees (10 or higher) and more trees in the ensemble (>100)
    
    paramGrid = ParamGridBuilder()\
        .addGrid(gbtr.maxDepth, [2, 5])\
        .addGrid(gbtr.maxIter, [10, 100])\
        .build()
    
    # We define an evaluation metric.  This tells CrossValidator how well we are doing by comparing the true labels with predictions.
    evaluator = RegressionEvaluator(metricName="rmse", labelCol=gbtr.getLabelCol(), predictionCol=gbtr.getPredictionCol())
    
    # Declare the CrossValidator, which runs model tuning for us.
    cv = CrossValidator(estimator=gbtr, evaluator=evaluator, estimatorParamMaps=paramGrid, parallelism=4)
    
    # define the pipeline including cross validation
    pipeline = Pipeline(stages=[hasher, assembler, cv])
    
    # training pipeline
    gbtr_model = pipeline.fit(train)
    
    # predictions on test data
    # gbtr_predictions = gbtr_model.transform(test)
    gbtr_predictions = gbtr_model.transform(test)
    display(predictions.select("cnt", "prediction", *featuresCols))
    
    # log model
    mlflow.spark.log_model(gbtr_model, "Weekly_Sales_GBTR_HPTuning_model")

    # Logging params handled by MLFlow autologger

    # Logging evaluation metric
  
    # log the rmse
    rmse = evaluator.evaluate(gbtr_predictions)
    mlflow.log_metric("RMSE", rmse)
    print("RMSE on our test set: %g" % rmse)
    

# COMMAND ----------


