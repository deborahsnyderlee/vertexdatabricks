# Databricks notebook source
# MAGIC %md-sandbox
# MAGIC <!-- <img src="https://databricks.com/wp-content/uploads/2021/02/gcp-blog-og.png" 
# MAGIC      style="width: 800px; height: 95px; object-fit: cover; object-position: 0 0;" > -->
# MAGIC # MLFlow Integration with and deployment to Vertex.AI
# MAGIC <img src="https://ebrire-databricks.s3.amazonaws.com/public/images/MLFlow-Vertex.png" width="950"><br>
# MAGIC 
# MAGIC Note: This demo is meant to address ML model serving as an API in GCP only. For a full end to end demo, see notebook here.

# COMMAND ----------

# DBTITLE 1,Import packages 📚
import mlflow
from mlflow.deployments import get_deploy_client
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_diabetes 
from sklearn.ensemble import RandomForestRegressor
import pandas as pd
import numpy as np

# COMMAND ----------

# DBTITLE 1,We'll create, train and test a simple model based on sklearn ⚙️
# load dataset
db = load_diabetes()
X = db.data
y = db.target
X_train, X_test, y_train, y_test = train_test_split(X, y)
 
# mlflow.sklearn.autolog() requires mlflow 1.11.0 or above.
mlflow.sklearn.autolog()
 
# With autolog() enabled, all model parameters, a model score, and the fitted model are automatically logged.  
with mlflow.start_run() as run:  
  # Set the model parameters. 
  n_estimators = 100
  max_depth = 6
  max_features = 3
  # Create and train model.
  rf = RandomForestRegressor(n_estimators = n_estimators, max_depth = max_depth, max_features = max_features)
  rf.fit(X_train, y_train)
  # Use the model to make predictions on the test dataset.
  predictions = rf.predict(X_test)
  
mlflow.end_run()

# COMMAND ----------

# DBTITLE 1,Log model in the MLFlow Registry 📝
model_name = "demo-vertex-sklearn"
mlflow.sklearn.log_model(rf, model_name, registered_model_name=model_name)

# COMMAND ----------

# MAGIC %md
# MAGIC <img src="https://ebrire-databricks.s3.amazonaws.com/public/images/registered-vertex-models.png" width=1100>

# COMMAND ----------

# DBTITLE 1,Since every rerun of this notebook creates a new model version, let's get the latest #️⃣ 🔢
client = mlflow.tracking.MlflowClient()
model_version_infos = client.search_model_versions(f"name = '{model_name}'")
model_version = max([int(model_version_info.version) for model_version_info in model_version_infos])

print(model_version)

# COMMAND ----------

# DBTITLE 1,Create Vertex client, print model URL
model_uri=f"models:/{model_name}/{model_version}"
displayHTML(f"<b><font size=5>MLFlow &nbsp;==> {model_uri}</b>")
displayHTML(f"<b><font size=5>Vertex.ai ==> {model_name}-{model_version}</b>")

# Really simple Vertex client instantiation
vtx_client = mlflow.deployments.get_deploy_client("google_cloud")

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC ## The next step handles everything
# MAGIC * Creates a deployment in Vertex
# MAGIC * Exports the model from MLFlow to Google Storage
# MAGIC * Imports the model from Google Storage
# MAGIC * Generates the image in Vertex
# MAGIC 
# MAGIC ## 🛑 It might take 22 minutes for the whole deployment to be finished
# MAGIC ## ⚠️ Other observations
# MAGIC * If `destination_image_uri` is not set, then `gcr.io/your-project/mlflow/your-vertex-model-name` will be used
# MAGIC * Your user/service account must have access to that storage location in Cloud Build

# COMMAND ----------

# DBTITLE 1,Heavy lifting happens here, MLFlow model gets deployed to Vertex
deploy_name = f"{model_name}-{model_version}"
deployment = vtx_client.create_deployment(
    name=deploy_name,
    model_uri=model_uri)

# COMMAND ----------

# MAGIC %md-sandbox
# MAGIC ## ✅ The model is now published in Vertex and it can be invoked in two different ways
# MAGIC * Directly as if it's still in the MLFlow registry (provided the client is `google_cloud`)<br>https://pypi.org/project/google-cloud-mlflow/
# MAGIC * Directly from Vertex once enabled as an endpoint serving a REST API (**Tested and Recommended**): <br>https://github.com/googleapis/python-aiplatform

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Invoke endpoint using Vertex AI python SDK
# MAGIC 
# MAGIC Pip install `google-cloud-aiplatform` or install directly into cluster.

# COMMAND ----------

deployments = vtx_client.list_deployments()
endpt_resource = [d["resource_name"] for d in deployments if d["name"] == deploy_name][0]
print(endpt_resource)

# COMMAND ----------

from google.cloud import aiplatform

aiplatform.init()
vtx_endpoint = aiplatform.Endpoint(endpt_resource)

arr = X_test.tolist()
pred = vtx_endpoint.predict(instances = arr)
pred
